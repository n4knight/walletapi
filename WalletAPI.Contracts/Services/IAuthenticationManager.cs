﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.DTOs;

namespace WalletAPI.Contracts.Services
{
    public interface IAuthenticationManager
    {
        Task<bool> ValidateUser(UserLoginDTO loginDto);
        Task<string> CreateToken();
    }
}
