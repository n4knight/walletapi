﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.DTOs;
using WalletAPI.Entities.Entities;

namespace WalletAPI.Contracts.Services
{
    public interface IUserService
    {
        Task<User> RegisterUser(RegistrationDTO registerDto);
        Task<Boolean> FundAccount(User user, FundAccountDTO fundAccountDto);
        Task<Boolean> PayBill(User user, PayBillsDTO paymentDto);
        Task<Boolean> Transfer(User user, TransferDTO transferDto);
        Task<string> LoginUser(UserLoginDTO loginDto);

        Task<User> GetUser(string username);
    }
}
