﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletAPI.Contracts.Repos;
using WalletAPI.Contracts.Services;
using WalletAPI.Entities.DTOs;
using WalletAPI.Entities.Entities;

namespace Wallet.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        private readonly IRepositoryBase<WalletAPI.Entities.Entities.Wallet> _accountRepo;
        private readonly IRepositoryBase<Transaction> _transactionRepo;
        private readonly IRepositoryManager _repoManager;
        public UserController(IUserService _userService, IRepositoryManager _repoManager)
        {
            this._userService = _userService;
            this._repoManager = _repoManager;
            _accountRepo = _repoManager.GetRepository<WalletAPI.Entities.Entities.Wallet>();
            _transactionRepo = _repoManager.GetRepository<Transaction>();
        }


        [HttpGet("")]
        public IActionResult initialTest()
        {
            return Ok("Smiling and Waving");
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser(RegistrationDTO registrationdto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Check input fields");
            }

            var user = await _userService.RegisterUser(registrationdto);

            if (user is null) return BadRequest("cannot create user");

            return Ok(user);
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginUser(UserLoginDTO loginDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Check input fields");
            }

            var token = await _userService.LoginUser(loginDto);

            if (token is null) return Unauthorized();

            HttpContext.Request.Headers.Add("Authorization", $"Bearer {token}");


            return Ok(token);

            
        }

        [HttpPost("/{username}/funding")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
        public async Task<IActionResult> FundAccount(string username, FundAccountDTO funding)
        {

            if (!ModelState.IsValid) return BadRequest();

            var isAccountFunded = await _userService.FundAccount(await _userService.GetUser(username), funding);

            if (!isAccountFunded)
                return NotFound("User wallet not found");

            return Ok(new { isAccountFunded = $"wallet Funded with {funding.Amount}" });

        }

        [HttpPost("/{username}/transfer")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
        public async Task<IActionResult> TransferFunds (string username, TransferDTO transferDTO)
        {
            if (!ModelState.IsValid) return BadRequest();

            var isTransferDone = await _userService.Transfer(await _userService.GetUser(username), transferDTO);

            if (!isTransferDone)
                return BadRequest("Insufficient funds");

            return Ok(new { isTransferDone = $"{transferDTO.Amount} was transfered to {transferDTO.ReceiversUserName}" });
        }

        [HttpPost("/{username}/paybills")]
        [Authorize(AuthenticationSchemes =JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
        public async Task<IActionResult> PayBills(string username, PayBillsDTO paybillDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var isPaymentDone = await _userService.PayBill(await _userService.GetUser(username), paybillDto);

            if (!isPaymentDone)
                return BadRequest("Insufficient funds");

            return Ok(new { isPaymentDone = $"You paid {paybillDto.Amount} for {paybillDto.PaymentFor}" });
        }


        
    }
}
