﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DTOs
{
    public class TransferDTO
    {
        [Required(ErrorMessage = "ReceiversUserName field is required")]
        public string ReceiversUserName { get; set; }

        [Required(ErrorMessage = "Amount field is required")]
        public Decimal Amount { get; set; }
    }
}
