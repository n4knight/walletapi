﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DTOs
{
    public class FundAccountDTO
    {
        [Required(ErrorMessage = "Amount Field is required")]
        public Decimal Amount { get; set; }
    }
}
