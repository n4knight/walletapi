﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DTOs
{
    public class PayBillsDTO
    {
        [Required(ErrorMessage = "Description field is required")]
        public string PaymentFor { get; set; }

        [Required(ErrorMessage = "Amount field is required")]
        public Decimal Amount { get; set; }
    }
}
