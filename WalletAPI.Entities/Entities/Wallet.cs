﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WalletAPI.Entities.Entities
{
    public class Wallet
    {
        public Guid WalletId { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public Decimal Balance { get; set; }

        public Guid WalletAddress { get; set; }
        public virtual User User { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
    }
}