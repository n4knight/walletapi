﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.Entities
{
    public class User: IdentityUser
    {
        public List<Transaction> Transactions { get; set; } = new List<Transaction>();

        //public Guid AccountID { get; set; }

        //public virtual Account Account { get; set; }
    }
}
