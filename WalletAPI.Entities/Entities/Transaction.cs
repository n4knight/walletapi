﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WalletAPI.Entities.Entities
{
    public class Transaction
    {
        public Guid TransactionId { get; set; }

        public string TransactionMode { get; set; }

        public virtual User User { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public Decimal Amount { get; set; }
    }
}