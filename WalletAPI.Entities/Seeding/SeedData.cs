﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.Entities;

namespace WalletAPI.Entities.Seeding
{
    public static class SeedData
    {
        public static void EnsureDataSeeded(WalletDbContext _context, UserManager<User> _userManager)
        {
            _context.Database.Migrate();

            var passwordHasher = new PasswordHasher<User>();

            var user1 = new User { UserName = "Kachi", Email = "Kachi@google.com", NormalizedUserName = "KACHI" };
            user1.PasswordHash = passwordHasher.HashPassword(user1, "kachi123456");

            var user2 = new User { UserName = "Pope", Email = "pope@google.com", NormalizedUserName = "POPE" };
            user2.PasswordHash = passwordHasher.HashPassword(user2, "pope123456");

            var user3 = new User { UserName = "Kcm", Email = "Kcm@google.com", NormalizedUserName = "KCM" };
            user3.PasswordHash = passwordHasher.HashPassword(user3, "kcm123456");

            var user4 = new User { UserName = "Alex", Email = "Alex@google.com", NormalizedUserName = "ALEX" };
            user4.PasswordHash = passwordHasher.HashPassword(user4, "Alex123456");

            if (!_context.Roles.Any())
            {
                _context.AddRange(new Roles[]
                {
                    new Roles
                    {
                        Id = "1",
                        Name = "User",
                        NormalizedName = "USER"
                    },
                    new Roles
                    {
                        Id = "2",
                        Name = "admin",
                        NormalizedName = "ADMIN"
                    }
                });

                _context.SaveChanges();
            }


            if (!_context.Users.Any())
            {
                _context.AddRange(new User[]
                {
                    user1,
                    user2,
                    user3,
                    user4
                });

                _context.SaveChanges();
            }

            var User1 = _context.Users.SingleOrDefault(u => u.UserName.Equals("Kachi") && u.Email.Equals("kachi@google.com"));
            var User2 = _context.Users.SingleOrDefault(u => u.UserName.Equals("Kcm") && u.Email.Equals("kcm@google.com"));
            var User3 = _context.Users.SingleOrDefault(u => u.UserName.Equals("Alex") && u.Email.Equals("Alex@google.com"));
            var User4 = _context.Users.SingleOrDefault(u => u.UserName.Equals("Pope") && u.Email.Equals("Pope@google.com"));

            if (!_context.UserRoles.Any())
            {
                _context.AddRange(new IdentityUserRole<string>[]
                {
                    new IdentityUserRole<string>
                    {
                        RoleId = "1",
                        UserId = user1.Id
                    },
                    new IdentityUserRole<string>
                    {
                        RoleId = "1",
                        UserId = user2.Id
                    },
                    new IdentityUserRole<string>
                    {
                        RoleId = "1",
                        UserId = user3.Id
                    },
                    new IdentityUserRole<string>
                    {
                        RoleId = "1",
                        UserId = user4.Id
                    }
                });
            }


            if (!_context.Wallets.Any())
            {
                _context.AddRange(new Wallet[]
                {
                    new Wallet
                    {
                        UserId = User1.Id,
                        User = user1,
                        WalletAddress = Guid.Parse("1CFD70AA-DDF0-464A-8A49-0D335C05CB68"),
                        Balance = 1_000_000
                    },
                    new Wallet
                    {
                        UserId = User2.Id,
                        User = user2,
                        WalletAddress = Guid.Parse("C36126C8-4BC1-4FDB-B97D-F4DB7B7C71E4"),
                        Balance = 1_000_000
                    },
                    new Wallet
                    {
                        UserId = User3.Id,
                        User = user3,
                        WalletAddress = Guid.Parse("2D966C39-2C3A-4AC5-9500-F4C32FA6626F"),
                        Balance = 1_000_000
                    },
                    new Wallet
                    {
                        UserId = User4.Id,
                        User = user4,
                        WalletAddress = Guid.Parse("30E4B2F3-0809-49F3-983C-EE2D26FEF26A"),
                        Balance = 1_000_000
                    }
                });

                _context.SaveChanges();
            }

        }
    }
}
