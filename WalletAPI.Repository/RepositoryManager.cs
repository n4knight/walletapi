﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contracts.Repos;
using WalletAPI.Entities;

namespace WalletAPI.Repository
{
    public class RepositoryManager<TContext> : IRepositoryManager<DbContext> where TContext: DbContext
    {
        private Dictionary<Type, object> _repositories;
        protected readonly WalletDbContext _walletcontext;

        public RepositoryManager(WalletDbContext _context)
        {
            this._walletcontext = _context ?? throw new ArgumentNullException(nameof(WalletDbContext));
        }


        public IRepositoryBase<T> GetRepository<T>() where T : class
        {
            if (_repositories is null) _repositories = new Dictionary<Type, object>();

            var type = typeof(T);
            if (!_repositories.ContainsKey(type)) _repositories[type] = new RepositoryBase<T>(_walletcontext);

            return (IRepositoryBase<T>)_repositories[type];
        }

        public int SaveChanges() => _walletcontext.SaveChanges();

        public async Task<int> SaveChangesAsync() => await _walletcontext.SaveChangesAsync();
    }
}
