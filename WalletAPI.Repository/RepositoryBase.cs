﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contracts.Repos;

namespace WalletAPI.Repository
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T: class
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<T> _dbSet;


        public RepositoryBase(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public async Task CreateAsync(T entity) => await _dbSet.AddAsync(entity);
        public void Create(T entity) =>
             _dbSet.Add(entity);

        public void Delete(T entity) =>
             _dbSet.Remove(entity);

        public IQueryable<T> FindAll() => _dbSet;

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression) =>
            _dbSet.Where(expression);

        public void Update(T entity) =>
            _dbSet.Update(entity);
    }
}
