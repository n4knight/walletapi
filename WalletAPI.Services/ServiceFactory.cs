﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contracts.Services;

namespace WalletAPI.Services
{
    public class ServiceFactory : IServiceFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public ServiceFactory(IServiceProvider _serviceProvider)
        {
            this._serviceProvider = _serviceProvider;
        }

        public T GetService<T>() where T : class
        {
            var service = _serviceProvider.GetService(typeof(T));
            return (T)service;
        }
    }
}
