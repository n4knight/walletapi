﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contracts.Repos;
using WalletAPI.Contracts.Services;
using WalletAPI.Entities.DTOs;
using WalletAPI.Entities.Entities;

namespace WalletAPI.Services
{
    public class UserService : IUserService
    {
        private readonly IRepositoryBase<User> _userRepo;
        private readonly IRepositoryBase<Wallet> _walletRepo;
        private readonly IRepositoryBase<Transaction> _transactionRepo;
        private readonly IRepositoryManager _repoManager;
        private readonly UserManager<User> _userManager;
        //private readonly RoleManager<Roles> _roleManager;
        //private readonly SignInManager<User> _signInManager;
        private readonly IAuthenticationManager _authManager;

        public UserService(IRepositoryManager _repoManager, UserManager<User> _userManager, 
            IAuthenticationManager _authManager)
        {
            this._repoManager = _repoManager;
            _userRepo = _repoManager.GetRepository<User>();
            _walletRepo = _repoManager.GetRepository<Wallet>();
            _transactionRepo = _repoManager.GetRepository<Transaction>();
            this._userManager = _userManager;
            this._authManager = _authManager;
            //this._roleManager = _roleManager;
        }

        public async Task<bool> FundAccount(User user, FundAccountDTO fundAccountDto)
        {
            var userWallet = await _walletRepo.FindByCondition(w => w.UserId.Equals(user.Id)).FirstOrDefaultAsync();

            if (userWallet != null)
            {
                userWallet.Balance += fundAccountDto.Amount;

                await _transactionRepo.CreateAsync(new Transaction
                {
                    UserId = userWallet.UserId,
                    TransactionMode = "ACCOUNT FUNDING",
                    Amount = fundAccountDto.Amount
                });

                await _repoManager.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<User> GetUser(string id) =>
            await _userManager.FindByNameAsync(id);

        public async Task<string> LoginUser(UserLoginDTO loginDto)
        {
            if (!await _authManager.ValidateUser(loginDto)) return null;
            return await _authManager.CreateToken();

        }

        public async Task<bool> PayBill(User user, PayBillsDTO paymentDto)
        {
            var userWallet = await _walletRepo.FindByCondition(w => w.UserId.Equals(user.Id)).FirstOrDefaultAsync();

            if (userWallet.Balance >= paymentDto.Amount)
            {
                userWallet.Balance -= paymentDto.Amount;
                await _transactionRepo.CreateAsync(new Transaction
                {
                    UserId = user.Id,
                    TransactionMode = "PAYMENT",
                    Amount = paymentDto.Amount
                });

                await _repoManager.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<User> RegisterUser(RegistrationDTO registerDto)
        {
            var user = new User
            {
                UserName = registerDto.UserName,
                Email = registerDto.Email
            };            

            var result = await _userManager.CreateAsync(user, registerDto.Password);
            await _userManager.AddToRoleAsync(user, "User");

            if (result.Succeeded) return await _userManager.FindByNameAsync(registerDto.UserName);

            return null;
        }

        public async Task<bool> Transfer(User user, TransferDTO transferDto)
        {
            var userWallet = await _walletRepo.FindByCondition(w => w.UserId.Equals(user.Id)).FirstOrDefaultAsync();

            var receiver = await _userManager.FindByNameAsync(transferDto.ReceiversUserName);
            var receiverWallet = await _walletRepo.FindByCondition(w => w.UserId.Equals(receiver.Id)).FirstOrDefaultAsync();

            if (transferDto.Amount <= userWallet.Balance)
            {
                receiverWallet.Balance += transferDto.Amount;

                await _transactionRepo.CreateAsync(new Transaction
                {
                    UserId = userWallet.UserId,
                    TransactionMode = "TRANSFER",
                    Amount = transferDto.Amount
                });

                await _repoManager.SaveChangesAsync();
                return true;
            }

            return false;

        }
    }
}
